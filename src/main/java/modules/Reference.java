package modules;

import javax.persistence.*;

@Entity
@Table(name = "reference")
public class Reference {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String issue_date;

    @OneToOne(fetch = FetchType.LAZY)
    private Student student;
}
