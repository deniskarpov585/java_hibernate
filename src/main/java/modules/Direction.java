package modules;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "direction")
public class Direction {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name ="name")
    private String name;

    @Column(name ="postponement")
    private boolean postponement;

    @OneToMany(mappedBy = "direction", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Student> students;

    public Direction(){
    }

    public Direction(String name, boolean postponement){
        this.name = name;
        this.postponement = postponement;
        students = new ArrayList<>();
    }

    public void addStudent(Student student){
        student.setDirection(this);
        students.add(student);
    }

    public void removeAuto(Student student) {
        students.remove(student);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isPostponement() {
        return postponement;
    }

    public void setPostponement(boolean postponement) {
        this.postponement = postponement;
    }

    public List<Student> getStudents() {
        return students;
    }

    public void setStudents(List<Student> students) {
        this.students = students;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "models.Direction{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", postponement=" + postponement +
                '}';
    }
}
