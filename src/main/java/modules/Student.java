package modules;

import javax.persistence.*;

@Entity
@Table(name = "student")
public class Student {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String passport;

    private String category;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_direction")
    private Direction direction;

    private String group;

    private String status;

    private int age;

    private String faculty;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_type")
    private Type type;

    @OneToOne(mappedBy = "student", cascade = CascadeType.ALL, optional = false, fetch = FetchType.LAZY)
    private Reference reference;

    public Student(){
    }

    public Student(String passport, String category, String group, String status,
                   int age, String faculty){
        this.passport = passport;
        this.category = category;
        this.group = group;
        this.status = status;
        this.age = age;
        this.faculty = faculty;
    }

    public String getPassport() {
        return passport;
    }

    public void setPassport(String passport) {
        this.passport = passport;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getFaculty() {
        return faculty;
    }

    public void setFaculty(String faculty) {
        this.faculty = faculty;
    }

    public int getId() {
        return id;
    }

    public Direction getDirection() {
        return direction;
    }

    public void setDirection(Direction direction) {
        this.direction = direction;
    }

    @Override
    public String toString() {
        return "models.Student{" +
                "id=" + id +
                ", passport='" + passport + '\'' +
                ", age=" + age +
                '}';
    }
}
