module com.example.course_db {
    requires java.naming;
    requires javafx.controls;
    requires javafx.fxml;
    requires org.hibernate.orm.core;
    requires javaee.api;

    opens com.example.course_db to javafx.fxml;
    exports com.example.course_db;
}