package services;

import dao.DirectionDao;
import modules.Direction;

public class DirectionService {
    private DirectionDao directionDao = new DirectionDao();

    public DirectionService(){
    }

    public Direction findUser(int id) {
        return directionDao.findById(id);
    }

    public void saveDirection(Direction direction) {
        directionDao.save(direction);
    }
}
