package dao;

import modules.Direction;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import utils.HibernateSessionFactoryUtil;

public class DirectionDao {
    SessionFactory factory = new Configuration().configure().addAnnotatedClass(Direction.class).buildSessionFactory();

    public Direction findById(int id) {
        return HibernateSessionFactoryUtil.getSessionFactory().openSession().get(Direction.class, id);
    }

    public void save(Direction direction) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.save(direction);
        tx1.commit();
        session.close();
    }
}
